SRC_URI = "git://anonscm.debian.org/collab-maint/ca-certificates.git \
           ${TWC_RDK_GIT_ROOT}/cmf-generic-sysint;protocol=${TWC_RDK_GIT_PROTOCOL};branch=${TWC_RDK_GIT_BRANCH};destsuffix=sysint;name=sysint \
           file://0001-update-ca-certificates-remove-c-rehash.patch \
           file://0002-update-ca-certificates-use-SYSROOT.patch \
           file://default-sysroot.patch \
           file://sbindir.patch"
