SUMMARY = "TPC Software Upgrade Manager Platform Headers"
LICENSE="CLOSED"

SRC_URI = "\
          git://git@bitbucket.org/st8018/swupmgr.git;protocol=ssh;branch=master \
          "
          
SRCREV = "${AUTOREV}"
PR = "r1"
S = "${WORKDIR}/git"

# this is a header file package only - nothing to build
do_compile[noexec] = "1"
do_configure[noexec] = "1"

# also get rid of the default dependency added in bitbake.conf
# since there is no 'main' package generated (empty)
RDEPENDS_${PN}-dev = ""

do_install() {
    INCLUDE_DIR=${STAGING_DIR_TARGET}${includedir}/tpc/swupmgr
    mkdir -p ${INCLUDE_DIR}
    cp -r ${S}/hal/include/*.h ${INCLUDE_DIR}/
    cp -r ${S}/iarm/include/*.h ${INCLUDE_DIR}/
    cp -r ${S}/iarm/src/*.h ${INCLUDE_DIR}/
    cp -r ${S}/include/*.h ${INCLUDE_DIR}/
}

export SYSROOT_INCDIR="${STAGING_DIR_TARGET}${includedir}"
export RDK_INCDIR="${STAGING_DIR_TARGET}${includedir}/rdk"
export TARGET_LIBDIR="${STAGING_DIR_TARGET}${libdir}"
export TARGET_BINDIR="${STAGING_DIR_TARGET}${bindir}"
export TPC_INCDIR="${STAGING_DIR_TARGET}${includedir}/tpc"




