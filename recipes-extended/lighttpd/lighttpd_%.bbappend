FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"
RDEPENDS_${PN} += " \
	libpcreposix \
	pcregrep \
	lighttpd-module-cgi \
	lighttpd-module-proxy-core \
	lighttpd-module-proxy-backend-http \
	lighttpd-module-proxy-backend-fastcgi \
	lighttpd-module-redirect \
	lighttpd-module-setenv \
	lighttpd-module-ssi \
	"

do_install_append_qemux86() {
	install -d ${D}/opt/www ${D}/opt/logs
}

FILES_${PN} += "/opt"
