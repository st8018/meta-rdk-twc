#
# ============================================================================
# COMCAST C O N F I D E N T I A L AND PROPRIETARY
# ============================================================================
# This file and its contents are the intellectual property of Comcast.  It may
# not be used, copied, distributed or otherwise  disclosed in whole or in part
# without the express written permission of Comcast.
# ============================================================================
# Copyright (c) 2014 Comcast. All rights reserved.
# ============================================================================
#
SUMMARY = "RDK fonts"
SECTION = "fonts"

LICENSE = "CLOSED"

PV = "${RDK_RELEASE}+gitr${SRCPV}"
SRC_URI = "${RDK_GENERIC_ROOT_GIT}/fonts/generic;protocol=${RDK_GIT_PROTOCOL};branch=${RDK_GIT_BRANCH}"
S = "${WORKDIR}/git"

SRCREV = "${AUTOREV}"

# we don't need a compiler nor a c library for these fonts
INHIBIT_DEFAULT_DEPS = "1"

do_install() {
    install -d ${D}${datadir}/fonts
    install -d ${D}${libdir}
    unzip -d ${D}${datadir}/fonts ${S}/XFINITYSansTT.zip
    ln -sf ${datadir}/fonts ${D}${libdir}/fonts

    install -d ${D}${sysconfdir}
    install -m 0644 ${S}/platform_fonts.conf ${D}${sysconfdir}/
}

FILES_${PN} = "${datadir}/fonts/*.ttf ${sysconfdir}/*.conf ${libdir}/fonts"

do_patch[noexec] = "1"
do_configure[noexec] = "1"
do_compile[noexec] = "1"

inherit allarch
