SUMMARY = "RDK Browser append file"
LICENSE = "CLOSED"

SRC_URI = "${TWC_RDK_GIT_ROOT}/cmf-generic-rdkbrowser.git;protocol=${TWC_RDK_GIT_PROTOCOL};branch=${TWC_RDK_GIT_BRANCH}"
SRC_URI += "${TWC_RDK_GIT_ROOT}/cmf-generic-servicemanager.git;protocol=${TWC_RDK_GIT_PROTOCOL};branch=${TWC_RDK_GIT_BRANCH};destsuffix=servicemanager;name=browserservicemanager"
SRC_URI += "${HTML_SRCURI}"

# TODO: The followings are from meta-rdk-comcast.
SRC_URI += "${RDK_GENERIC_ROOT_GIT}/rdkapps/generic;protocol=${RDK_GIT_PROTOCOL};branch=${RDK_GIT_BRANCH};destsuffix=git/rdkapps;name=rdkbrowserapps"

FILES_${PN} += "/webserver/*"
FILES_${PN} += "${bindir}/*"

#uncomment these below lines in rdk-generic-cef-images to enable CEF in rdkbrowser

#DEPENDS += "rdkcefapp"
#export CEF_STAGE_DIR := "${STAGING_DIR_TARGET}/usr"


do_install_append() {
        install -d ${D}/webserver/www
        rm -rf ${S}/rdkapps/html/ExtMenu.html
        rm -rf ${S}/rdkapps/html/usb
        cp ${S}/startBrowser.yocto ${S}/startBrowser

        install -m 0755 ${S}/startBrowser ${D}${bindir}/
        install -m 0644 ${S}/rdkapps/conf/* ${D}/webserver/
        install -m 0644 ${S}/rdkapps/html/* ${D}/webserver/www/
}
