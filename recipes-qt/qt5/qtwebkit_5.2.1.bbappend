require qt5-twc.inc

FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}-${PV}:"

# if DLNA is disabled, we use plain qtwebkit, with no RMF depends, and we disable DLNA in the patches
# if playersinkbin (generic mediaplayersink) is disabled, we depend on SoC mediaplayersink
PACKAGECONFIG = "gstreamer010"
DEPENDS_remove = "qtdeclarative"

# We do not want to apply any patches.
# Instead, all necessary patches should be pushed into the git repo.
do_patch[noexec] = "1"
