require qt5-twc.inc

PACKAGECONFIG_GL = "gles2"
PACKAGECONFIG_FB = " "
PACKAGECONFIG_DISTRO = "icu examples sql-sqlite glib gstreamer pcre"

PACKAGECONFIG_remove = "dbus"

PACKAGECONFIG[printsupport] = ",-DQT_NO_PRINTER,,"

FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}-${PV}:"

# We do not want to apply any patches.
# Instead, all necessary patches should be pushed into the git repo.
do_patch[noexec] = "1"

do_install_append() {
        install -d ${D}${datatdir}
        rm -rf ${D}${libdir}/fonts/*.pfb
        mv ${D}${libdir}/fonts ${D}${datadir}
	rm -rf ${D}${libdir}/cacert.pem
}

FILES_${PN}-fonts-ttf-vera       = "${datadir}/fonts/Vera*.ttf"
FILES_${PN}-fonts-ttf-dejavu     = "${datadir}/fonts/DejaVu*.ttf"
FILES_${PN}-fonts-pfa            = "${datadir}/fonts/*.pfa"
FILES_${PN}-fonts-qpf            = "${datadir}/fonts/*.qpf*"
FILES_${PN}-fonts                = "${datadir}/fonts/README \
                                    ${datadir}/fonts/fontdir"

PACKAGES =. "\
    ${PN}-test \
    ${PN}-printsupport \
    ${PN}-sql \
    ${PN}-xml \
"

FILES_${PN}-test = "${libdir}/libQt5Test.so.5.*"
FILES_${PN}-printsupport = "${libdir}/libQt5PrintSupport.so.5.*"
FILES_${PN}-sql = "${libdir}/libQt5Sql.so.5.*"
FILES_${PN}-xml = "${libdir}/libQt5Xml.so.5.*"
