QT_MODULE_BRANCH ?= "master"
QT_REPO_BASE ?= "twc-broadcom"
QTVER = "5.2.1"

SRC_URI = "${TWC_RDK_GIT_ROOT}/${QT_REPO_BASE}-${QT_MODULE}-${QTVER}.git;protocol=ssh;branch=${QT_MODULE_BRANCH}"

SRCREV = "${AUTOREV}"

S = "${WORKDIR}/git"
