SUMMARY = "TPC Software Upgrade Manager Platform implementation"
LICENSE="CLOSED"

DEPENDS += "rdk-logger iarmbus swupmgr-headers"

SRC_URI = "\
          git://git@bitbucket.org/st8018/swupmgrhal.git;protocol=ssh;branch=master \
          "
          
SRCREV = "${AUTOREV}"
S = "${WORKDIR}/git"

export SWUPMGRHAL_ROOTDIR="${S}"

export SYSROOT_INCDIR="${STAGING_DIR_TARGET}${includedir}"
export RDK_INCDIR="${STAGING_DIR_TARGET}${includedir}/rdk"
export TARGET_LIBDIR="${STAGING_DIR_TARGET}${libdir}"
export TARGET_BINDIR="${STAGING_DIR_TARGET}${bindir}"
export TPC_INCDIR="${STAGING_DIR_TARGET}${includedir}/tpc"


# include so file in release ipk
FILES_SOLIBSDEV = ""
FILES_${PN} += "${libdir}"
FILES_${PN} += "${bindir}"

inherit cmake


