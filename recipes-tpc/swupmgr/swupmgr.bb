SUMMARY = "TPC Software Upgrade Manager"
LICENSE="CLOSED"

DEPENDS += "rdk-logger iarmbus swupmgr-headers swupmgrhal"

SRC_URI = "\
          git://git@bitbucket.org/st8018/swupmgr.git;protocol=ssh;branch=master \
          "
          
SRCREV = "${AUTOREV}"
S = "${WORKDIR}/git"

export SWUPMGR_ROOTDIR="${S}"
export SYSROOT_INCDIR="${STAGING_DIR_TARGET}${includedir}"
export RDK_INCDIR="${STAGING_DIR_TARGET}${includedir}/rdk"
export TARGET_LIBDIR="${STAGING_DIR_TARGET}${libdir}"
export TARGET_BINDIR="${STAGING_DIR_TARGET}${bindir}"
export TPC_INCDIR="${STAGING_DIR_TARGET}${includedir}/tpc"

inherit cmake

do_install() {
    install -d ${D}${bindir}
    install -m 0755 ${B}/swupMgrMain ${D}${bindir}
    install -m 0755 ${B}/swupMgrTest ${D}${bindir}
}

FILES_${PN} += "${bindir}/*"
