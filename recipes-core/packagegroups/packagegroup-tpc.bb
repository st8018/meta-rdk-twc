# Copyright 2015 Time Warner Cable, Inc.
#
# This module contains unpublished, confidential, proprietary
# material.  The use and dissemination of this material are
# governed by a license.  The above copyright notice does not
# evidence any actual or intended publication of this material.

SUMMARY = "Custom package group for TPC components"

LICENSE = "CLOSED"

DESCRIPTION = "TPC Package Group"

inherit packagegroup

PACKAGES = "\
         packagegroup-tpc \
         "

RDEPENDS_packagegroup-tpc = "\
    swupmgr  \
    swupmgrhal  \
    "
    
require tpc.inc
# packagegroup-rdk-generic-mediaclient 

